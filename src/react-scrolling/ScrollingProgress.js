import gsap from "gsap";
import { ScrollTrigger } from "gsap/all";
import { useEffect } from "react";
import "./style.css";
import { useAuth0 } from "@auth0/auth0-react";

export const ScrollingProgress = () => {
  const { loginWithRedirect, isAuthenticated, user, logout } = useAuth0();
  useEffect(() => {
    gsap.registerPlugin(ScrollTrigger);
    gsap.to("progress", {
      value: 100,
      scrollTrigger: {
        scrub: 0.5,
      },
    });
  }, []);

  return (
    <>
      <progress max={100} value={0}></progress>
      <nav>
        <h1>Dashboard</h1>
        {isAuthenticated ? (
          <button
            onClick={() =>
              logout({ logoutParams: { returnTo: window.location.origin } })
            }
          >
            Log Out
          </button>
        ) : (
          <button onClick={() => loginWithRedirect()}>Log In</button>
        )}
      </nav>
      <div className="container">
        <section>
          {isAuthenticated ? (
            <div style={{paddingTop:"100px"}}>
              <img src={user.picture} alt={user.name} />
              <h2>{user.name}</h2>
              <p>{user.email}</p>
            </div>
            
          ) : <p style={{paddingTop:"100px"}}>Try login functionality 💋</p>}
        </section>
        <section></section>
        <section></section>
      </div>
    </>
  );
};
